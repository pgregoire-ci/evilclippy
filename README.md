# EvilClippy

Automated build of the `EvilClippy` utility at
https://github.com/outflanknl/EvilClippy

Package can be download at:

https://gitlab.com/pgregoire-ci/evilclippy/-/jobs/artifacts/main/download?job=build
